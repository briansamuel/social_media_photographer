import 'package:social_media_photographers/blocs/login/login_bloc.dart';
import 'package:social_media_photographers/blocs/register/register_state.dart';
import 'package:social_media_photographers/components/already_have_an_account_acheck.dart';
import 'package:social_media_photographers/components/rounded_button.dart';
import 'package:social_media_photographers/components/rounded_input_field.dart';
import 'package:social_media_photographers/components/rounded_password_field.dart';

import 'package:social_media_photographers/repositories/user_repository.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:social_media_photographers/blocs/register/register_bloc.dart';
import 'package:social_media_photographers/blocs/register/register_event.dart';
import 'package:social_media_photographers/views/login/login_screen.dart';

import 'components/background.dart';
import 'components/or_divider.dart';
import 'components/social_icon.dart';

class RegisterScreen extends StatefulWidget {
  final UserRepository _userRepository;

  // contructor
  RegisterScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passController = TextEditingController();

  RegisterBloc _registerBloc;
  UserRepository get _userRepository => widget._userRepository;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _emailController.addListener(() {
      //
      _registerBloc
          .add(RegisterEventEmailChanged(email: _emailController.text));
    });
    _passController.addListener(() {
      //
      _registerBloc
          .add(RegisterEventPasswordChanged(password: _passController.text));
    });
  }

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passController.text.isNotEmpty;

  bool isRegisterButtonEnable(RegisterState registerState) =>
      registerState.isValidEmailAndPassword &&
      isPopulated &&
      !registerState.isSubmitting;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocBuilder<RegisterBloc, RegisterState>(
          builder: (context, registerState) {
        if (registerState.isFailure) {
          print('Register failed');
        } else if (registerState.isSubmitting) {
          print('Registering');
        } else if (registerState.isSuccess) {
          print('Register Success and Login');
          Navigator.of(context).pop();
        }

        return Background(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "SIGNUP",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: size.height * 0.03),
                SvgPicture.asset(
                  "assets/icons/signup.svg",
                  height: size.height * 0.35,
                ),
                RoundedInputField(
                  hintText: "Your Email",
                  controller: _emailController,
                  validatorText:
                      registerState.isValidEmail ? null : 'Invalid email',
                ), // Email Input
                RoundedPasswordField(
                  controller: _passController,
                  validatorText:
                      registerState.isValidPassword ? null : 'Invalid password',
                ), // Password Input
                RoundedButton(
                  text: "SIGNUP",
                  press: isRegisterButtonEnable(registerState)
                      ? _onRegisterEmailAndPassword
                      : null,
                ), // Register Button
                SizedBox(height: size.height * 0.03),
                AlreadyHaveAnAccountCheck(
                  login: false,
                  press: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) {
                        return BlocProvider<LoginBloc>(
                          create: (context) =>
                              LoginBloc(userRepository: _userRepository),
                          child: LoginScreen(
                            userRepository: _userRepository,
                          ),
                        );
                      }),
                    );
                  },
                ),
                OrDivider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SocalIcon(
                      iconSrc: "assets/icons/facebook.svg",
                      press: () {},
                    ),
                    SocalIcon(
                      iconSrc: "assets/icons/twitter.svg",
                      press: () {},
                    ),
                    SocalIcon(
                      iconSrc: "assets/icons/google-plus.svg",
                      press: () {},
                    ),
                  ],
                ) // Social Login button
              ],
            ),
          ),
        );
      }),
    );
  }

  void _onRegisterEmailAndPassword() {
    _registerBloc.add(RegisterEventPressed(
      email: _emailController.text,
      password: _passController.text,
    ));
  }
}
