import 'package:social_media_photographers/blocs/authentication/authentication_bloc.dart';
import 'package:social_media_photographers/blocs/authentication/authentication_event.dart';

import 'package:social_media_photographers/blocs/login/login_bloc.dart';
import 'package:social_media_photographers/blocs/login/login_event.dart';
import 'package:social_media_photographers/blocs/login/login_state.dart';

import 'package:social_media_photographers/components/already_have_an_account_acheck.dart';
import 'package:social_media_photographers/components/rounded_button.dart';
import 'package:social_media_photographers/components/rounded_input_field.dart';
import 'package:social_media_photographers/components/rounded_password_field.dart';

import 'package:social_media_photographers/repositories/user_repository.dart';
import '../register/components/or_divider.dart';
import '../register/components/social_icon.dart';
import '../register/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import 'components/background.dart';

// import 'components/body.dart';

class LoginScreen extends StatefulWidget {
  final UserRepository _userRepository;

  // contructor
  LoginScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passController = TextEditingController();

  LoginBloc _loginBloc;
  UserRepository get _userRepository => widget._userRepository;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(() {
      //
      _loginBloc.add(LoginEventEmailChanged(email: _emailController.text));
    });
    _passController.addListener(() {
      //
      _loginBloc.add(LoginEventPasswordChanged(password: _passController.text));
    });
  }

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passController.text.isNotEmpty;

  bool isLoginButtonEnable(LoginState loginState) =>
      loginState.isValidEmailAndPassword &&
      isPopulated &&
      !loginState.isSubmitting;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocBuilder<LoginBloc, LoginState>(builder: (context, loginState) {
        if (loginState.isFailure) {
          print('Login failed');
        } else if (loginState.isSubmitting) {
          print('Login in');
        } else if (loginState.isSuccess) {
          print('Login Success');
          BlocProvider.of<AuthenticationBloc>(context)
              .add(AuthenticationEventLoggedIn());
        }
        return Background(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "LOGIN",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: size.height * 0.03),
                SvgPicture.asset(
                  "assets/icons/login.svg",
                  height: size.height * 0.35,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedInputField(
                  hintText: "Your Email",
                  controller: _emailController,
                  validatorText:
                      loginState.isValidEmail ? null : 'Invalid email format',
                ), // Input Email
                RoundedPasswordField(
                  controller: _passController,
                  validatorText:
                      loginState.isValidPassword ? null : 'Invalid pass format',
                ), // Input Password
                RoundedButton(
                  text: "LOGIN",
                  press: isLoginButtonEnable(loginState)
                      ? _onLoginEmailAndPassword
                      : null,
                ), // Button Login
                SizedBox(height: size.height * 0.03),
                AlreadyHaveAnAccountCheck(
                  press: () {
                    // Navigator.of(context).push(
                    //   MaterialPageRoute(builder: (context) {
                    //     return BlocProvider<RegisterBloc>(
                    //       create: (context) =>
                    //           RegisterBloc(userRepository: _userRepository),
                    //       child: RegisterScreen(
                    //         userRepository: _userRepository,
                    //       ),
                    //     );
                    //   }),
                    // );
                  },
                ), // Component Route to Sign In or Sign Out
                OrDivider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SocalIcon(
                      iconSrc: "assets/icons/facebook.svg",
                      press: () {},
                    ),
                    SocalIcon(
                      iconSrc: "assets/icons/twitter.svg",
                      press: () {},
                    ),
                    SocalIcon(
                      iconSrc: "assets/icons/google-plus.svg",
                      press: _onLoginGoogleAccount,
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  void _onLoginEmailAndPassword() {
    _loginBloc.add(LoginEventWithEmailAndPasswordPressed(
      email: _emailController.text,
      password: _passController.text,
    ));
  }

  void _onLoginGoogleAccount() {
    _loginBloc.add(LoginEventWithGooglePressed());
  }
}
