class Api {
  String baseUrl = 'https://psocial.xyz/api/v1/';
  String prefixAuth = 'auth/';
  String prefixAlbum = 'albums/';
  String prefixStory = 'stories/';
  String prefixUser = 'users/';
  String prefixFriends = 'friends';
  String prefixFeeds = 'feeds/';
  String prefixComments = 'comments/';
}
