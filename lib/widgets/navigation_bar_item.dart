import 'package:flutter/material.dart';

class NavBarItem extends StatelessWidget {
  final Function onTapNavItem;
  final IconData icon;
  final int index;
  final int selectedItemIndex;
  final double width;
  const NavBarItem({
    @required Function this.onTapNavItem,
    @required IconData this.icon,
    @required int this.index,
    @required int this.selectedItemIndex,
    @required double this.width,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapNavItem,
      child: Container(
        width: 82,
        height: 45,
        child: icon != null
            ? Icon(
                icon,
                size: 25,
                color: index == selectedItemIndex
                    ? Colors.black
                    : Colors.grey[700],
              )
            : Container(),
      ),
    );
  }
}
