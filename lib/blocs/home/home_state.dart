import 'package:equatable/equatable.dart';
import 'package:social_media_photographers/models/feeds.dart';
import 'package:social_media_photographers/models/story.dart';
import 'package:social_media_photographers/models/user.dart';

abstract class HomeState extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class HomeStateInitial extends HomeState {}

class HomeStateLoaded extends HomeState {
  List<Feeds> feeds;
  List<User> friends;
  List<Story> stories;

  @override
  // TODO: implement props
  List<Object> get props => [feeds, friends, stories];
}

class HomeStateFailed extends HomeState {}
