import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class HomeEventInitial extends HomeEvent {}

class HomeEventLoading extends HomeEvent {}

class HomeEventLoaded extends HomeEvent {}

class HomeEventFailure extends HomeEvent {}
