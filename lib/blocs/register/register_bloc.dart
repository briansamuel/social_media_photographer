import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';

import 'package:rxdart/rxdart.dart';
import 'package:social_media_photographers/blocs/register/register_event.dart';
import 'package:social_media_photographers/blocs/register/register_state.dart';
import 'package:social_media_photographers/repositories/user_repository.dart';
import 'package:social_media_photographers/validators/validtors.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  RegisterBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(RegisterState.initial());
  // Giới hạn thời gian 2 lần nhấn button
  @override
  Stream<Transition<RegisterEvent, RegisterState>> transformEvents(
      Stream<RegisterEvent> registerEvent,
      TransitionFunction<RegisterEvent, RegisterState> transitionFn) {
    // TODO: implement transformEvents
    // Những sự kiện cần delay thời gian nhấn

    final debounceStream = registerEvent.where((registerEvent) {
      return (registerEvent is RegisterEventEmailChanged ||
          registerEvent is RegisterEventPasswordChanged);
    }).debounceTime(const Duration(
        milliseconds: 300)); // Giới hạn 3s cho sự kiện - yêu cầu rxdart

    // Những sự kiện không cần delay thời gian nhấn
    final nonDebounceStream = registerEvent.where((registerEvent) {
      return (registerEvent is RegisterEventPressed);
    });

    // Merge 2 luồng với nhau - Rxdart
    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent registerEvent) async* {
    // TODO: implement mapEventToState
    final registerState = state;

    if (registerEvent is RegisterEventEmailChanged) {
      // Sự kiện thay đổi Mail
      yield registerState.cloneAdnUpdate(
          isValidEmail: Validators.isValidEmail(registerEvent.email));
    } else if (registerEvent is RegisterEventPasswordChanged) {
      // Sự kiện thay đổi Pass
      yield registerState.cloneAdnUpdate(
          isValidPassword: Validators.isValidPassword(registerEvent.password));
    } else if (registerEvent is RegisterEventPressed) {
      // Sự kiện button nhấn đăng ký
      try {
        await _userRepository.createUserWithEmailAndPassword(
            registerEvent.email, registerEvent.password);
        yield RegisterState.success();
      } catch (_) {
        yield RegisterState.failure();
      }
    }
  }
}
