import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
  @override
  // TODO: implement props
  List<Object> get props => [];
}

// Sự kiện thay đổi text email input
class RegisterEventEmailChanged extends RegisterEvent {
  final String email;
  //contructor
  const RegisterEventEmailChanged({this.email});

  @override
  // TODO: implement props
  List<Object> get props => [email];

  @override
  String toString() => 'Email changed: $email';
}

// Sự kiện thay đổi text password input
class RegisterEventPasswordChanged extends RegisterEvent {
  final String password;
  //contructor
  const RegisterEventPasswordChanged({this.password});

  @override
  // TODO: implement props
  List<Object> get props => [password];

  @override
  String toString() => 'Password changed: $password';
}

// Sự kiện khi click đăng ký
class RegisterEventPressed extends RegisterEvent {
  final String email;
  final String password;

  RegisterEventPressed({this.email, this.password});

  @override
  // TODO: implement props
  List<Object> get props => [email, password];

  @override
  String toString() =>
      'RegisterEventPressed email: $email, password: $password';
}
