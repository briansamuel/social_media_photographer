import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:social_media_photographers/repositories/user_repository.dart';
import './authentication_event.dart';
import './authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;
  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(AuthenticationStateInitial());
  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent authenticationEvent) async* {
    // TODO: implement mapEventToState
    try {
      if (authenticationEvent is AuthenticationEventStarted) {
        final isSignedIn = await _userRepository.isSignedIn();
        if (isSignedIn) {
          final currentUser = await _userRepository.getUser();
          yield AuthenticationStateSuccess(user: currentUser);
        } else {
          yield AuthenticationStateFailure();
        }
      } else if (authenticationEvent is AuthenticationEventLoggedIn) {
        final currentUser = await _userRepository.getUser();
        yield AuthenticationStateSuccess(user: currentUser);
      } else if (authenticationEvent is AuthenticationEventLoggedOut) {
        _userRepository.signOut();
        yield AuthenticationStateFailure();
      } else {
        yield AuthenticationStateFailure();
      }
    } catch (exception) {
      yield AuthenticationStateFailure();
    }
  }
}
