import 'package:equatable/equatable.dart';
import 'package:social_media_photographers/models/user.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AuthenticationStateInitial extends AuthenticationState {}

class AuthenticationStateFailure extends AuthenticationState {}

class AuthenticationStateSuccess extends AuthenticationState {
  final User user;
  const AuthenticationStateSuccess({this.user});

  @override
  // TODO: implement props
  List<Object> get props => [user];

  @override
  String toString() => 'AuthenticationStateSuccess, email: $user';
}
