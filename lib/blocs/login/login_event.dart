import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
  @override
  // TODO: implement props
  List<Object> get props => [];
}

// Sự kiện thay đổi text email input
class LoginEventEmailChanged extends LoginEvent {
  final String email;
  //contructor
  const LoginEventEmailChanged({this.email});

  @override
  // TODO: implement props
  List<Object> get props => [email];

  @override
  String toString() => 'Email changed: $email';
}

// Sự kiện thay đổi text password input
class LoginEventPasswordChanged extends LoginEvent {
  final String password;
  //contructor
  const LoginEventPasswordChanged({this.password});

  @override
  // TODO: implement props
  List<Object> get props => [password];

  @override
  String toString() => 'Password changed: $password';
}

// sự kiện khi click đăng nhập bằng Google
class LoginEventWithGooglePressed extends LoginEvent {}

class LoginEventWithEmailAndPasswordPressed extends LoginEvent {
  final String email;
  final String password;

  LoginEventWithEmailAndPasswordPressed({this.email, this.password});

  @override
  // TODO: implement props
  List<Object> get props => [email, password];

  @override
  String toString() =>
      'LoginEventWithEmailAndPasswordPressed email: $email, password: $password';
}
