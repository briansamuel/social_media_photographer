import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:social_media_photographers/blocs/login/login_event.dart';
import 'package:social_media_photographers/blocs/login/login_state.dart';
import 'package:social_media_photographers/repositories/user_repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:social_media_photographers/validators/validtors.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository _userRepository;
  LoginBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(LoginState.initial());

  // Giới hạn thời gian 2 lần nhấn button
  @override
  Stream<Transition<LoginEvent, LoginState>> transformEvents(
      Stream<LoginEvent> loginEvent,
      TransitionFunction<LoginEvent, LoginState> transitionFn) {
    // TODO: implement transformEvents
    // Những sự kiện cần delay thời gian nhấn

    final debounceStream = loginEvent.where((loginEvent) {
      return (loginEvent is LoginEventEmailChanged ||
          loginEvent is LoginEventPasswordChanged);
    }).debounceTime(const Duration(
        milliseconds: 500)); // Giới hạn 3s cho sự kiện - yêu cầu rxdart

    // Những sự kiện không cần delay thời gian nhấn
    final nonDebounceStream = loginEvent.where((loginEvent) {
      return (loginEvent is LoginEventWithEmailAndPasswordPressed ||
          loginEvent is LoginEventWithGooglePressed);
    });

    // Merge 2 luồng với nhau - Rxdart
    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent loginEvent) async* {
    // TODO: implement mapEventToState
    final loginState = state;
    if (loginEvent is LoginEventEmailChanged) {
      yield loginState.cloneAdnUpdate(
          isValidEmail: Validators.isValidEmail(loginEvent.email));
    } else if (loginEvent is LoginEventPasswordChanged) {
      yield loginState.cloneAdnUpdate(
          isValidPassword: Validators.isValidEmail(loginEvent.password));
    } else if (loginEvent is LoginEventWithGooglePressed) {
      try {
        // await _userRepository.signInWithGoole();
        yield LoginState.success();
      } catch (_) {
        yield LoginState.failure();
      }
    } else if (loginEvent is LoginEventWithEmailAndPasswordPressed) {
      try {
        await _userRepository.signInWithEmailAndPassword(
            loginEvent.email, loginEvent.password);
        yield LoginState.success();
      } catch (_) {
        yield LoginState.failure();
      }
    }
  }
}
