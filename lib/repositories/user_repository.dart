import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_media_photographers/configs/api.dart';
import 'package:social_media_photographers/models/user.dart';
import 'package:social_media_photographers/repositories/auth_repository.dart';
import 'package:http/http.dart' as http;

class UserRepository {
  final AuthRepository _authRepository;
  final http.Client _httpClient;
  String _token;
  DateTime _expiryDate;
  String _userId;
  Timer _authTimer;
  User _currentUser;

  //contructor
  UserRepository({AuthRepository authRepository, http.Client httpClient})
      : _authRepository = authRepository ?? AuthRepository(),
        _httpClient = httpClient ?? http.Client();

  Future<void> signInWithEmailAndPassword(String email, String password) async {
    return await _authRepository.signInWithEmailAndPassword(email, password);
  }

  Future<void> createUserWithEmailAndPassword(
      String email, String password) async {
    return await _authRepository.createUserWithEmailAndPassword(
        email, password);
  }

  Future<bool> isSignedIn() async {
    return await _authRepository.isSignedIn();
  }

  Future<User> getUser() async {
    return await _authRepository.getUser();
  }

  Future<void> signOut() async {
    return await _authRepository.logout();
  }
}
