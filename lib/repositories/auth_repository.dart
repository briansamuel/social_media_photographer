import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_media_photographers/models/user.dart';
import '../configs/api.dart';

class AuthRepository {
  http.Client _httpClient = http.Client();
  String _token;
  DateTime _expiryDate;
  Timer _authTimer;
  User _currentUser;

  // Function signInWithEmailAndPassword
  Future<void> signInWithEmailAndPassword(String email, String password) async {
    // Login http request
    final url = Uri.parse(Api().baseUrl + Api().prefixAuth + 'login');
    try {
      final response = await _httpClient.post(
        url,
        body: {
          'email': email,
          'password': password,
        },
      );

      // decode reponse body
      final responseData = json.decode(response.body);
      // check error
      if (responseData['error'] != null) {
        throw Exception(responseData['error']);
      }

      // set token, expiryDate
      _token = responseData['access_token'];
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: responseData['expires_in'],
        ),
      );
      // Autologout
      _autoLogout();
      // Store data local
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode(
          {'token': _token, 'expiryDate': _expiryDate.toIso8601String()});
      prefs.setString('userData', userData);
    } on TimeoutException catch (_) {
      // A timeout occurred.
    } on SocketException catch (_) {
      // Other exception
    } catch (error) {
      throw (error);
    }
  }

  // Function createUserWithEmailAndPassword
  Future<void> createUserWithEmailAndPassword(
      String email, String password) async {
    final url = Uri.parse(Api().baseUrl + Api().prefixAuth + 'register');
    try {
      final response = await http.post(
        url,
        body: json.encode(
          {
            'email': email,
            'password': password,
          },
        ),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw Exception(responseData['error']['message']);
      }
    } catch (error) {
      //
      throw (error);
    }
  }

  // Function getUser from Auth Token
  Future<User> getUser() async {
    final url = Uri.parse(Api().baseUrl + Api().prefixAuth + 'user');

    try {
      final response = await _httpClient.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $_token',
        },
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw Exception(responseData['error']);
      }
      _currentUser = User.fromJson(responseData);
      return _currentUser ?? null;
    } catch (error) {
      throw (error);
    }
  }

  // Function check user is logged
  Future<bool> isSignedIn() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);
    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserData['token'];
    _expiryDate = expiryDate;
    return true;
  }

  // Function login user when reload ap
  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);
    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserData['token'];
    _expiryDate = expiryDate;
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _currentUser = null;
    _expiryDate = null;

    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    final _timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: _timeToExpiry), logout);
  }
}
