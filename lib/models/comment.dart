import 'package:equatable/equatable.dart';
import 'package:social_media_photographers/models/react.dart';
import 'package:social_media_photographers/models/user.dart';

class Comment extends Equatable {
  final int id;
  final User user;
  final DateTime createdAt;
  final String content;
  final List<React> listReact;
  final bool isYourReact;
  final List<Comment> childComment;

  const Comment(this.id, this.user, this.createdAt, this.content,
      this.listReact, this.isYourReact, this.childComment);
  @override
  // TODO: implement props
  List<Object> get props =>
      [id, user, createdAt, content, listReact, isYourReact, childComment];
}
