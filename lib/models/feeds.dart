import 'package:equatable/equatable.dart';
import 'package:social_media_photographers/models/comment.dart';
import 'package:social_media_photographers/models/react.dart';

import 'user.dart';

class Feeds extends Equatable {
  final int id;
  final User user;
  final DateTime createdAt;
  final String status;
  final List<ImageFeed> imageFeed;
  final List<React> listReact;
  final List<Comment> comments;
  final bool isYourReact;

  const Feeds(this.id, this.user, this.createdAt, this.status, this.imageFeed,
      this.listReact, this.comments, this.isYourReact);

  @override
  // TODO: implement props
  List<Object> get props => [
        id,
        user,
        createdAt,
        status,
        imageFeed,
        listReact,
        comments,
        isYourReact
      ];
}

class ImageFeed extends Equatable {
  final int id;
  final User user;
  final DateTime createdAt;
  final String status;
  final String imageUrl;
  final List<React> listReact;
  final List<Comment> comments;
  final bool isYourReact;

  ImageFeed(this.id, this.user, this.createdAt, this.status, this.imageUrl,
      this.listReact, this.comments, this.isYourReact);

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, user, createdAt, status, imageUrl, listReact, comments, isYourReact];
}
