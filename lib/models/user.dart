import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import 'location.dart';

@immutable
class User extends Equatable {
  final int id;
  final String fullname;
  final Location location;
  final String avatarUrl;
  final DateTime birthDate;
  final DateTime lastLogin;
  User({
    @required this.id,
    @required this.fullname,
    @required this.location,
    @required this.avatarUrl,
    @required this.birthDate,
    @required this.lastLogin,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      fullname: json['full_name'],
      location: null,
      avatarUrl: json['avatar'],
      birthDate: DateTime.parse(json['birth_date']),
      lastLogin: DateTime.parse(json['last_login']),
    );
  }

  User copyWith({
    int id,
    String fullname,
    Location location,
    String avatarUrl,
    DateTime birthDate,
    DateTime lastLogin,
  }) {
    return User(
        id: id ?? this.id,
        fullname: fullname ?? this.fullname,
        location: location ?? this.location,
        avatarUrl: avatarUrl ?? this.avatarUrl,
        birthDate: birthDate ?? this.birthDate,
        lastLogin: lastLogin ?? this.lastLogin);
  }

  User fromJson(Map<String, dynamic> json) {
    return copyWith(
      id: json['id'],
      fullname: json['full_name'],
      location: json['current_location']
          ? new Location(
              latitude: json['current_location']['latitude'],
              longitude: json['current_location']['longitude'],
              address: json['current_location']['address'],
            )
          : null,
      avatarUrl: json['avatar'],
      birthDate: DateTime.parse(json['birth_date']),
      lastLogin: DateTime.parse(json['last_login']),
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [id, fullname, location, avatarUrl, birthDate];
}
