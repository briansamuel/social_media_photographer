import 'package:equatable/equatable.dart';
import 'package:social_media_photographers/models/user.dart';

enum TypeReact {
  Like,
  Love,
  Heart,
  Angry,
}

class React extends Equatable {
  final int id;
  final User user;
  final TypeReact type;

  React(this.id, this.user, this.type);

  @override
  // TODO: implement props
  List<Object> get props => [id, user, type];
}
