import 'package:equatable/equatable.dart';
import 'package:social_media_photographers/models/user.dart';

class Story extends Equatable {
  final int id;
  final User user;
  final String content;
  final String imageUrl;
  final String musicUrl;

  Story(this.id, this.user, this.content, this.imageUrl, this.musicUrl);

  @override
  // TODO: implement props
  List<Object> get props => [id, user, content, imageUrl, musicUrl];
}
