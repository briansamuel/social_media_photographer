import 'package:social_media_photographers/components/text_field_container.dart';
import 'package:social_media_photographers/styles/constants.dart';
import 'package:flutter/material.dart';

class RoundedPasswordField extends StatelessWidget {
  final TextEditingController controller;
  final String validatorText;
  const RoundedPasswordField({Key key, this.controller, this.validatorText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFieldContainer(
          child: TextFormField(
            controller: controller,
            cursorColor: kPrimaryColor,
            decoration: InputDecoration(
              hintText: "Password",
              icon: Icon(
                Icons.lock,
                color: kPrimaryColor,
              ),
              suffixIcon: Icon(
                Icons.visibility,
                color: kPrimaryColor,
              ),
              border: InputBorder.none,
            ),
            obscureText: true,
            autovalidate: true,
            autocorrect: false,
          ),
        ),
        AnimatedContainer(
          child: Text(
            validatorText ?? '',
            style: TextStyle(
              color: Colors.red,
              fontSize: 12,
            ),
          ),
          height: validatorText != null ? 20 : 0,
          duration: Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
        )
      ],
    );
  }
}
