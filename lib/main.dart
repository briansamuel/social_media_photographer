import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:social_media_photographers/blocs/authentication/authentication_event.dart';
import 'package:social_media_photographers/blocs/home/home_bloc.dart';
import 'package:social_media_photographers/blocs/home/home_event.dart';

import 'package:social_media_photographers/repositories/user_repository.dart';

import 'blocs/authentication/authentication_bloc.dart';
import 'blocs/authentication/authentication_state.dart';
import 'blocs/home/home_state.dart';
import 'blocs/login/login_bloc.dart';
import 'views/home/home_screen.dart';
import 'views/login/login_screen.dart';
import 'views/splash_screen.dart';

void main() {
  runApp(SocialMedia());
}

class SocialMedia extends StatelessWidget {
  final UserRepository _userRepository = UserRepository();
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (BuildContext context) =>
              AuthenticationBloc(userRepository: _userRepository)
                ..add(AuthenticationEventStarted()),
        ),
        BlocProvider<HomeBloc>(
          create: (BuildContext context) => HomeBloc(HomeStateInitial()),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: BlocProvider(create: (context) {
          final authenticationBloc =
              AuthenticationBloc(userRepository: _userRepository);
          authenticationBloc.add(AuthenticationEventStarted());
          return authenticationBloc;
        }, child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, authenticationState) {
            if (authenticationState is AuthenticationStateSuccess) {
              return HomeScreen();
            } else if (authenticationState is AuthenticationStateFailure) {
              return BlocProvider<LoginBloc>(
                create: (context) => LoginBloc(userRepository: _userRepository),
                child: LoginScreen(userRepository: _userRepository),
              );
            }
            return SplashScreen();
          },
        )),
      ),
    );
    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   home: HomePage(),
    // );
  }
}
